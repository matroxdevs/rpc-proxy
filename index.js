require('dotenv').config();

const http = require('http');
const httpProxy = require('http-proxy');

const FORWARD_URL = process.env.FORWARD_URL;
const PORT = process.env.PORT || 666;

const proxy = httpProxy.createProxyServer({
  target: FORWARD_URL,
  changeOrigin: true
});

proxy.on('error', (err, req, res) => {
  console.error(`Error making HTTP request: ${err.message}`);
  res.writeHead(500, {'Content-Type': 'text/plain'});
  res.end('Internal server error');
});

const httpServer = http.createServer((req, res) => {
  proxy.web(req, res);
});

httpServer.listen(PORT, () => {
  console.log(`HTTP proxy server listening on port ${PORT} and forwarding to ${FORWARD_URL}`);
});

